# MultiLingualCrowsPairs

This is the Github repo for Multilingual CrowS-Pairs, a challenge dataset for measuring stereotypical biases present in the masked language models (MLMs) in 7 different languages. This challenge dataset was built on the Crows-Pairs corpus (Nangia et al. 2020) using the methodology described in (Névéol et al. 2023). The 7 new languages are the following:

- Arabic from Maghreb and the Arab world in general
- Catalan from Spain
- German from Germany
- Spanish from Argentina
- Italian from Italy
- Maltese from Malta
- simplified Chinese from China

In addition to this new dataset, we addressed issues reported by Blodgett et al. (2021) in previously created english and french contents.

## The dataset

Here are listed the different files composing our dataset. Each file consists of examples covering ten types of biases: race/color, gender/gender identity, sexual orientation, religion, age, nationality, disability, physical appearance, socioeconomic status, and “other”.

- catalan_spain_crowspairs.csv : 1677 pairs for Catalan from Spain (ca_ES)
- german_germany_crowspairs.csv : 1677 pairs for German from Germany (de_DE)
- english_US_crowspairs.csv : 1677 pairs for english from US (en_US)
- spanish_argentina_crowspairs.csv : 1506 pairs for Spanish from Argentina (es_AR)
- french_france_crowspairs.csv : 1677 pairs for french from France (fr_FR)
- italian_italy_crowspairs.csv : 1705 pairs for Italian from Italy (it_IT)
- maltese_malta_crowspairs.csv : 1677 pairs for Maltese from Malta (mt_MT)
- simplified_chinese_china_crowspairs.csv : 1481 pairs for simplified Chinese from China (zh_CN)

## How we evaluated our datasets

Each language used to some extent its own modification of the [original Crows-Pairs scripts](https://github.com/nyu-mll/crows-pairs). If you would like to learn more about these scripts or if you would like to use them, please refer to the [indicated github page](https://github.com/nyu-mll/crows-pairs) and to [(Nangia et al. 2020)](https://aclanthology.org/2020.emnlp-main.154/). 

Alternatively, you can use the code proposed by [(Névéol et al. 2022)](https://gitlab.inria.fr/french-crows-pairs/acl-2022-paper-data-and-code) which is a modified version of the original Crows-Pairs scripts and where the implementation of new LMs might be easier: in **metric.py** line 16 you will find a dictionnary named **lmname2model**. Delete each LM in it that you don't want to use and add new LMs you wish to evaluate.

Note that if you wish to evaluate a language other than English, French or one of the languages covered by this document, you will need to translate the original Crows-Pairs dataset into that language.

## License

Multilingual CrowS-Pairs is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. It is created using material developed by the authors of the Crows-Pairs corpus (Nangia et al. 2020).

## Reference
